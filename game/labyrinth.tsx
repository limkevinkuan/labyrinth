<?xml version="1.0" encoding="UTF-8"?>
<tileset name="labyrinth" tilewidth="16" tileheight="16" tilecount="2" columns="2">
 <image source="labyrinth.png" width="32" height="16"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16">
    <properties>
     <property name="collidable" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="1" y="1" width="14" height="14">
    <properties>
     <property name="collidable" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
</tileset>
