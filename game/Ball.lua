local levity = require "levity"

local Ball = class()
local CameraSize = 256

function Ball:_init(object)
	self.body = object.body
	self.body:setFixedRotation(true)
	self.forces = {0, 0}
	local cx, cy = self.body:getWorldCenter()
	levity.camera:set(cx, cy, CameraSize, CameraSize)
end

function Ball:setForce(axis, force)
	self.forces[axis] = self.forces[axis] and force
end

function Ball:joystickaxis(joystick, axis, value)
	self:setForce(axis, value*CameraSize/2)
end

function Ball:mousemoved(x, y, dx, dy)
	x, y = levity:screenToCamera(x, y)
	x = x + levity.camera.x
	y = y + levity.camera.y
	local cx, cy = self.body:getWorldCenter()
	self:setForce(1, x - cx)
	self:setForce(2, y - cy)
end

function Ball:beginMove(dt)
	self.body:applyForce(self.forces[1], self.forces[2])
end

function Ball:endMove(dt)
	levity.camera:set(self.body:getWorldCenter())
end

function Ball:keypressed(key)
	if key == "escape" then
		love.event.quit()
	end
end

return Ball
